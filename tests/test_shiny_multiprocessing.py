import unittest
from shiny_multiprocessing import shiny_multiprocessing as mww

from typing import List
from time import sleep
from concurrent.futures import TimeoutError


def task_square(my_int: int):
    print(f'My int: {my_int}')
    return my_int ** 2


def task_square_with_fails(my_int: int, my_fails: List[Exception]):
    print(f'My int: {my_int}')
    print(f'My fails: {my_fails}')
    raise ValueError("I don't want to compute")


def my_super_lazy_task():
    sleep(4)


class MyTestCase(unittest.TestCase):
    def test_normal(self):
        # given

        # when
        expected = {1, 4, 9, 16}
        results = mww.process_and_retry(
            max_workers=3,
            function=task_square,
            items=[{'my_int': i} for i in range(1, 5)],
        )

        # then
        self.assertEqual(set(results), expected)

    def test_with_fails(self):
        # given

        # when

        # then
        self.assertRaises(
            mww.TooManyRetriesException,
            mww.process_and_retry,
            max_workers=3,
            function=task_square_with_fails,
            pass_previous_attempts_key='my_fails',
            items=[{'my_int': i} for i in range(1, 5)],
            retry_on_exceptions=[ValueError, KeyError]
        )

    def test_with_excluded_fails(self):
        # given

        # when

        # then
        self.assertRaises(
            ValueError,
            mww.process_and_retry,
            max_workers=3,
            function=task_square_with_fails,
            pass_previous_attempts_key='my_fails',
            items=[{'my_int': i} for i in range(1, 5)],
            retry_on_exceptions=[KeyError]
        )

    def test_timeout(self):
        # given

        # when

        # then
        self.assertRaises(
            TimeoutError,
            mww.process_and_retry,
            max_workers=3,
            timeout=2.5,
            function=my_super_lazy_task,
            items=[{} for i in range(1, 5)]
        )


if __name__ == '__main__':
    unittest.main()
